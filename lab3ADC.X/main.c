W#pragma config OSC = HS         // Oscillator Selection bits (XT oscillator)
#pragma config PWRT = ON        // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOR = OFF        // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config LVP = OFF        // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)


#define _XTAL_FREQ 20000000
#include <xc.h>
#include <stdio.h>
#include "LCD.h"

/*
 
--> Primero se configura ADCON1 para inciializar los pines en PORTA a coneveniencia si usaremos el ADC.
--> reloj de converson se seleccion en ADCON0, se escoje menor al periodop de muestreo (periodo se�al) (ley adquisicion)
--> EL hecho de tener mas de 7 canales no implica 7 conversores, solo hay 1 y se multiplexa el canal a seleccion.
--> justiifacion iszquiera y derecha
  
 ADRESH      ADRESL
 rrrr rrrr   rr000000 --> justificacion izquierda (left)
 0000 00rr   rrrrrrrr --> justificacion derecha (right)

  
 */
unsigned short result_adc;
float tempADC;
char buff[30], buff2[30];

void boot() {
    lcdInit();

    // config interrupts.

    INTCONbits.GIE = 1; // enable global interrupts.
    INTCONbits.PEIE = 1; // enable peripheral inerrupts.

    // configuracion ADC.

    PIE1bits.ADIE = 1; // enable interrupt AD.
    PIR1bits.ADIF = 0; // clear flag interrupt AD.
    ADCON1 = 0x80; // JUstifiacion derecha, Fosc/2, all pins in PORTA and PORTE are analogs.
    ADCON0 = 0x01; // Fosc/2, canal 0, ADC esta encendido.    

}

void readADC(void) {
    __delay_us(60); // Time of acquisition.
    ADCON0bits.GO = 1; // start conversion with bit GO.
}

/*
 
Vref+ = Vdd = 5v
Vref- = Vss = GND

 * 
scale = 5V / (2^10) = 4.82mV 
  
------------------
 TEST-1
 * ValueADC: 768 --> 3.75V --> MULTIMETER: 3.8V.
 * 
 * Aproximamos el valor de escala a 4.949mV aribitramriamente.
  
 
 */

void main(void) {
    boot();
    __delay_ms(2000);
    lcdwriteString("HOLA");
    lcdsetCursor(1, 1);
    lcdwriteString("Temp: ");
    lcdsetCursor(2, 1);
    lcdwriteString("Voltaje: ");
    short int cont = 0;
    __delay_ms(2000);
    while (1) {
        cont++;
        readADC();
        sprintf(buff2, "Samples: %d", cont);
        lcdsetCursor(2, 1);
        lcdwriteString(buff2);

        __delay_ms(50);


    }

    return;
}

void __interrupt() adint(void) {
    if (PIR1bits.ADIF == 1) {
        PIR1bits.ADIF = 0;
        result_adc = ADRESH << 8;
        result_adc |= ADRESL;
        tempADC = result_adc * 4.949e-3;
        sprintf(buff, "ValueADC: %f", tempADC);
        lcdsetCursor(1, 1);
        lcdwriteString(buff);

        ADRESH = 0x00;
        ADRESL = 0x00;
    }
}
