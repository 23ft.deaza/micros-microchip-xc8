#include <xc.h>


#define LCD_EN_Delay 500
#define dataPORT TRISD
#define pinRS TRISC2
#define pinEN TRISC3
#define RS LATCbits.LATC2
#define EN LATCbits.LATC3
#define bus LATD
 
void lcdInit(); // Initialize The LCD For 4-Bit Interface
void lcdClear(); // Clear The LCD Display
void lcdSl(); // Shift The Entire Display To The Left
void lcdSr(); // Shift The Entire Display To The Right
void enable(void); // enable pin to write data.
 
void lcdCmd(unsigned char); // Send Command To LCD
void lcdsetCursor(unsigned char, unsigned char); // Set Cursor Position
void lcdwriteChar(char); // Write Character To LCD At Current Position
void lcdwriteString(char*); // Write A String To LCD

