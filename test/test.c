/*
 * File:   main.c
 * Author: 23ft
 * Task: PWM.
 * Created on 16 de abril de 2022, 10:58 AM
 */
#include <stdio.h>

// (PWM period / (Tcy * TMR2 prescale value)) - 1 = PR2
#define period_pr2(pwm_period, frec_osc, presc_tmr2) (pwm_period / ((4/frec_osc) * presc_tmr2)) - 1 

// PWM duty cycle / TOSC * (TMR2 prescale value) = (CCPR1L:CCP1CON<5:4>) 
#define duty_ccp(pwm_period, pwm_duty, frec_osc, presc_tmr2) (((pwm_duty * pwm_period)/100) / ((1/frec_osc) * presc_tmr2))



unsigned int mask = 0x03;

void main(void) {
    
    float pwm_p = 1e0/2.4e3;

    printf("Pwm period PR2: %f", period_pr2(pwm_p, 20e6, 16e0));
    printf("Pwm duty   CCP: %f", duty_ccp(pwm_p, 50.0, 20e6, 16.0));
    return;
}
