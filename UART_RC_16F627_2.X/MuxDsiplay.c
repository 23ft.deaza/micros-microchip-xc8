/* 

 * Module for multiplexing displays, for 4 digits.
 * created by 23ft 
 
*/
// #include <xc.h>
// #include <pic16f628a.h>


/* Prototypes Functions */
void muxInit();                                     // Inicializacion PIC en modo MuxDisplays.
void muxNumber(unsigned num, unsigned numDigits);   // Sepárador de numero en unidades y decenas para visualizar. 
void muxView()                                      // Mostrar numero en displays.

/* Display 7 seg const */
#define segA2F PORTB          // Segments A to F
#define segG PORTAbits.RA0    // Segment G

#define D1 PORTAbits.RA2 // control encendido display 1 (decenas)
#define D0 PORTAbits.RA1 // control encendido display 0 (unidades)

unsigned char numbers[10][2] = {{0xF9, 0},  // Num0
                                 {0x18, 0}, // Num1
                                 {0x69, 1}, // Num2
                                 {0x39, 1}, // Num3
                                 {0x98, 1}, // Num4
                                 {0xB1, 1}, // Num5
                                 {0xF1, 1}, // Num6
                                 {0x19, 0}, // Num7
                                 {0xF9, 1}, // Num8
                                 {0xB9, 1}};// Num9


void muxInit() {
    TRISA = 0x00;   // TRISA A1-A2 are inputs.
    TRISB = 0x06;   // TRISB B1-B2 are inputs for USART.
    CMCON = 0x07;   // RA0-RA3 Digital I/O
}

void muxView(unsigned digitsView){
    D1 = 0;
    D0 = 1;
    
    segA2F = numbers[uni][0];
    segG = numbers[uni][1];
    __delay_ms(10);
    
    D1 = 1;
    D0 = 0;
    
    segA2F = numbers[dec][0];
    segG = numbers[dec][1];
    __delay_ms(10);
    
}

void muxNumber(unsigned num, unsigned numDigits){
    static unsigned uni, dec, res;
    
    dec = (num - (num % 10)) / 10;
    res = num % 10;
    uni = res;
    muxView(numDigits);
}

