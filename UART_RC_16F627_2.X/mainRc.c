
// PIC16F628A Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator: High-speed crystal/resonator on RA6/OSC2/CLKOUT and RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF      // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is digital input, MCLR internally tied to VDD)
#pragma config BOREN = OFF      // Brown-out Detect Enable bit (BOD disabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB4/PGM pin has digital I/O function, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdlib.h>
#include <string.h>


#define _XTAL_FREQ 16000000
#include "dispMux.c"

char dataRc[30],  dataRc_c;
unsigned long lenRcD, dataNum, DRXF = 0;
unsigned char rcRead();

void bootPic(){
    TRISA = 0x00;
    CMCON = 0x07;   // RA0-RA3 Digital I/O
}

void initRcUART(){
    TRISBbits.TRISB1 = 1;   // RX pin (RB1) init in input.
    TRISBbits.TRISB2 = 1;   // TX pin (RB2) init in input.
    
    SPBRG = 103;            // 103 apropiate baud rate. FOR 9600
    BRGH = 1;
    
    SYNC= 0;
    SPEN = 1;
    
    INTCONbits.GIE = 1;     // Enabled global interrupt bit. enables all un-masked interrupts
    INTCONbits.PEIE = 1;    // Enabled peripheral interrup bit, enables all un-masked peripheral interrupts.
    CREN = 1;               // bit for enabled the reception.
    RCIE = 1;               // enabled the interrupt for bit RCIF.
}

unsigned char rcRead(){
    while(!RCIF){
        continue;
    }
    dataRc_c = RCREG;         // RCIF is clear in hardware, depend of a state of RCREG     
    return dataRc_c;
}

void rcReadString(char *out, unsigned int size){   
    for(int i = 0; i < size; i++){
        out[i] = rcRead();         
    }
}

void main(void) {
    bootPic();
    initRcUART();
    muxInit();
    //rcRead();
    
    unsigned long probe = 60, cont = 0;
    char *ref;
    
    while(1){
        
        
        rcReadString(dataRc, 2);

        
        dataNum = strtoul(dataRc, &ref, 10);
        
        do{
            muxNumber(dataNum, 2);
            cont++;
        }while(cont < 1000);
        cont = 0;
        
                
    }
        
    return;
}

/*
void __interrupt() ISR(){
    
    
    if(RCIF){
                
        rcReadString(dataRc, 2);
        
        if (RCSTAbits.OERR == 1){
            RCSTAbits.CREN = 0;
            __delay_ms(25);
            RCSTAbits.CREN = 1;
        }
        
       
    }    
}
*/
