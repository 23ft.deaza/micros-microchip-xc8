#include "LCD.h"

void lcdCmd(unsigned char CMD)
{
  // Select Command Register
  RS = 0;
  // Move The Command Data To LCD
  bus = CMD;
  // Send The EN Clock Signal
  enable();
}

void lcdClear()
{
  lcdCmd(1);
}

void lcdsetCursor(unsigned char r, unsigned char c)
{
  unsigned char Temp,Low4,High4;
  if(r == 1)
  {
    Temp = 0x80 + c - 1; //0x80 is used to move the cursor
    lcdCmd(Temp);
  }
  
  if(r == 2)
  {
    Temp = 0xC0 + c - 1;
    lcdCmd(Temp);
  }
}

void lcdInit (void)
{
    // IO Pin Configurations
    dataPORT = 0x00;
    pinRS, pinEN = 0;
    
    
    
	__delay_ms(20);     // delay 20 ms
	bus=0x30;			// VALOR DE INICIALIZACION
	enable();

	__delay_ms(5);		// delay 5 ms
	bus=0x30;			// VALOR DE INICIALIZACION
	enable();

	__delay_ms(1);		// delay 1 ms
	bus=0x30;			// VALOR DE INICIALIZACION
	enable();

	bus=0b00111000;	// Function set 8 bits - 2 lineas - 5*8
	enable();

	//bus=0b00001000;	// display off
	//enable();

	//bus=0b00000001;	// display clear
	//enable();

	//bus=0b00000110;	// entry mode set - incrementa la posicion del cursor
	//enable();

	bus=0b00001110;	// display on/off control - LCD on - cursor on - blink off
	enable();
    
    
}

void lcdwriteChar(char Data)
{
  RS = 1;
  bus = Data;
  enable();
  __delay_ms(1);
}
void enable(void){
    EN = 1;
    __delay_us(LCD_EN_Delay);
    EN = 0;
    __delay_us(LCD_EN_Delay);
}

void lcdwriteString(char *str)
{
  for(int i=0;str[i]!='\0';i++)
    lcdwriteChar(str[i]);
}

void lcdSl()
{
  lcdCmd(0x18);
  
}
void lcdSr()
{
  lcdCmd(0x1C);
}