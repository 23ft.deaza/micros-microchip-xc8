/*
 * File:   main.c
 * Author: pipe
 *
 * Created on 14 de marzo de 2022, 01:35 PM
 */


/* 
 
 SPRINTF(buffer, string and options, origen)
  * retorna tama�o del string
  * ademas de ello sive de formateador.

 %d % i --> int
 %c --< char
 %u --> unsigned int
 %s --> string
 %x --> int hexadecimal
 %X --> int hexadecimal en mayus
 %F --> float
 %e --> float not-acion cientifica
 %E --> float notacion cientifica en mayus

  
 pantalla proteus: LM016L
 * 
 * mi primera casio.
*/

#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config OSCS = OFF       // Oscillator System Clock Switch Enable bit (Oscillator system clock switch option is disabled (main oscillator is source))
// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = OFF        // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config BORV = 20        // Brown-out Reset Voltage bits (VBOR set to 2.0V)
// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 128      // Watchdog Timer Postscale Select bits (1:128)
// CONFIG3H
#pragma config CCP2MUX = OFF    // CCP2 Mux bit (CCP2 input/output is multiplexed with RB3)
// CONFIG4L
#pragma config STVR = OFF       // Stack Full/Underflow Reset Enable bit (Stack Full/Underflow will not cause RESET)
#pragma config LVP = OFF        // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)
// CONFIG5L
#pragma config CP0 = OFF         // Code Protection bit (Block 0 (000200-001FFFh) code protected)
#pragma config CP1 = OFF        // Code Protection bit (Block 1 (002000-003FFFh) not code protected)
#pragma config CP2 = OFF        // Code Protection bit (Block 2 (004000-005FFFh) not code protected)
#pragma config CP3 = OFF        // Code Protection bit (Block 3 (006000-007FFFh) not code protected)
// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0001FFh) not code protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code protected)
// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit (Block 0 (000200-001FFFh) not write protected)
#pragma config WRT1 = OFF       // Write Protection bit (Block 1 (002000-003FFFh) not write protected)
#pragma config WRT2 = OFF       // Write Protection bit (Block 2 (004000-005FFFh) not write protected)
#pragma config WRT3 = OFF       // Write Protection bit (Block 3 (006000-007FFFh) not write protected)
// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0001FFh) not write protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write protected)
// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit (Block 0 (000200-001FFFh) not protected from Table Reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit (Block 1 (002000-003FFFh) not protected from Table Reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit (Block 2 (004000-005FFFh) not protected from Table Reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit (Block 3 (006000-007FFFh) not protected from Table Reads executed in other blocks)
// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0001FFh) not protected from Table Reads executed in other blocks)


#include <xc.h>
#define _XTAL_FREQ 20000000
#include <stdlib.h>
#include <stdio.h>
#include <pic18f452.h>
#include "LCD.h"

#define dataIn PORTB 
#define operationMode PORTA 
#define dataOut LATD
#define speak LATDbits.LATD7

unsigned char A, B;
short int value;

void boot(void);
unsigned char nums(void);
unsigned char sum(void);
unsigned char rest(char sig);
unsigned char product(char bcd);
unsigned char divi(void);
unsigned char and(void);
unsigned char or(void);
unsigned char xor(void);
unsigned char nand(void);
unsigned char nor(void);
unsigned char xnor(void);
unsigned char shiftLeft(void);
unsigned char shiftRight(void);
unsigned char do8(void);
unsigned char fa8(void);

void boot(void) {
    ADCON1 = 0x06; // Registro ADCON1 para ADC, configuracion pines como I/O
    TRISB = 0xFF; // Registro TRISB definimos todos como entradas.
    TRISA = 0xFF; // Registro TRISA definimos todas como entradas.
    TRISD = 0x00; // Registro TRISC definimos todos como salidas.
    TRISC = 0x00;
}

void main(void) {
    boot();
    nums();
    unsigned char val;

    while (1) {
        //nums();
        val = 0;
        switch (operationMode) {
            case 0: // RA0

                //PORTDbits.RD0 = 1;
                nums();
                sum();
                
                

                break;

            case 1: // RA1
                //PORTDbits.RD1 = 1;
                nums();
                val = rest(0);
                dataOut = val;
                val = 0;
                break;

            case 3: // RA2                
                //PORTDbits.RD3 = 1;
                //dataOut = divi();
                val = divi();
                dataOut = val;
                val = 0;

                break;

            case 2: // RA3
                //PORTDbits.RD4 = 1;
                //dataOut = product(0);
                val = product(0);
                dataOut = val;
                val = 0;


                break;

            case 4: // RA4
                //PORTDbits.RD5 = 1;
                //dataOut = and();
                val = and();
                dataOut = val;
                val = 0;
                break;

            case 5: // RA5
                //PORTDbits.RD6 = 1;
                //dataOut = or();
                val = or();
                dataOut = val;
                val = 0;
                break;

            case 6: // RA6

                //dataOut = xor();
                val = xor();
                dataOut = val;
                val = 0;
                break;
            case 7:


                val = nand();
                dataOut = val;
                val = 0;
                break;

            case 8:

                //dataOut = nor();
                val = nor();
                dataOut = val;
                val = 0;
                break;


            case 9:

                //dataOut = xnor();
                val = xnor();
                dataOut = val;
                val = 0;
                break;
            case 10:

                //dataOut = shiftLeft();
                val = shiftLeft();
                dataOut = val;
                val = 0;
                break;

            case 11:

                //dataOut = shiftRight();
                val = shiftRight();
                dataOut = val;
                val = 0;
                break;

            case 12:

                //dataOut = rest(1); // a-b con signo
                val = rest(1);
                dataOut = val;
                val = 0;

                break;
            case 13:

                //dataOut = product(1); // a*b bcd
                val = product(1);
                dataOut = val;
                val = 0;

                break;

            case 14:

                
                do{ 
                    // DO 4 OCTAVA = F{261.63Hz} --> T{3.822191} ms --> 50% 1.911095822 ms
                    LATDbits.LATD0 = 1;
                    __delay_ms(1.911095822);
                    LATDbits.LATD0 = 0;
                    __delay_ms(1.911095822);
                }while(operationMode == 14);
                break;

            case 15:

                do{
                    // FA 4 OCTAVA = F{349.23} --> 50% 1.431721215 ms
                    LATDbits.LATD0 = 1;
                    __delay_ms(1.43043266615); //1.431721215 - 1.41740400309 - 1.42456260917 - 1.4274260516 - 1.43028949403 - 1.43100535464 - 1.43043266615 - 1.4305758382723
                    LATDbits.LATD0 = 0; 

                    __delay_ms(1.43043266615);
                }while(operationMode == 15);
                
                break;
        }
    }

    return;
}

unsigned char nums() {
    A = dataIn & 0xF0;
    A >>= 4;
    B = dataIn & 0x0F;
    //A = 0x05;
    //B = 0x05;
}

unsigned char sum() {
    unsigned char temp = A + B;
    return temp;
}

unsigned char rest(char sig) {
    if (sig) {
        value = 0;
        value = A - B;

        if (value < 0) {
            value *= -1;

            value |= 0x80;
            //printf("%d\n", value);
        }
    } else {
        value = 0;
        //printf("A: %d B: %d\n", A, B);
        value = A - B;
        
        if (value < 0){
            return 0x00;
        }

    }
    return value;
}

unsigned char product(char bcd) {
    dataOut = 0x00;
    unsigned char value2 = 0;
    value2 = 0;
    value2 = A*B;
    //printf("value: %d\n", value);

    if (bcd) {
        if (value2 <= 99) {
            unsigned char mask = 0x00;
            char d = 0, u = 0;
            u = value2 % 10;
            d = ((value2 % 100) - u) / 10;

            //printf("value u: %d\n value d: %d\n", u, d);            

            mask |= d;
            mask <<= 4;
            mask |= u;
            //printf("mask: %d\n", mask);
            return mask;

        } else {
            return 0x00;
        }

    } else {

        return value2;
    }

}

unsigned char divi(void) {
    return A / B;
}

unsigned char and(void) {
    return A & B;
}

unsigned char or(void) {
    return A | B;
}

unsigned char nand(void) {
    //dataOut = 0x00;
    return ~((A) & (B));
}

unsigned char xor(void) {
    return A ^ B;
}

unsigned char nor(void) {
    return ~(A | B);
}

unsigned char xnor(void) {
    return ~(A ^ B);
}

unsigned char shiftLeft(void) {
    return ((dataIn << 7) | ((dataIn & 0xFE) >> 1));
}

unsigned char shiftRight(void) {
    return ((dataIn >> 5) | ((dataIn & 0x1F) << 3));
}



 