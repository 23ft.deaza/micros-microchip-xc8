// PIC18F452 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config OSCS = OFF       // Oscillator System Clock Switch Enable bit (Oscillator system clock switch option is disabled (main oscillator is source))

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = OFF        // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config BORV = 20        // Brown-out Reset Voltage bits (VBOR set to 2.0V)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 128      // Watchdog Timer Postscale Select bits (1:128)

// CONFIG3H
#pragma config CCP2MUX = OFF    // CCP2 Mux bit (CCP2 input/output is multiplexed with RB3)

// CONFIG4L
#pragma config STVR = OFF       // Stack Full/Underflow Reset Enable bit (Stack Full/Underflow will not cause RESET)
#pragma config LVP = OFF        // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit (Block 0 (000200-001FFFh) not code protected)
#pragma config CP1 = OFF        // Code Protection bit (Block 1 (002000-003FFFh) not code protected)
#pragma config CP2 = OFF        // Code Protection bit (Block 2 (004000-005FFFh) not code protected)
#pragma config CP3 = OFF        // Code Protection bit (Block 3 (006000-007FFFh) not code protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0001FFh) not code protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit (Block 0 (000200-001FFFh) not write protected)
#pragma config WRT1 = OFF       // Write Protection bit (Block 1 (002000-003FFFh) not write protected)
#pragma config WRT2 = OFF       // Write Protection bit (Block 2 (004000-005FFFh) not write protected)
#pragma config WRT3 = OFF       // Write Protection bit (Block 3 (006000-007FFFh) not write protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0001FFh) not write protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit (Block 0 (000200-001FFFh) not protected from Table Reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit (Block 1 (002000-003FFFh) not protected from Table Reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit (Block 2 (004000-005FFFh) not protected from Table Reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit (Block 3 (006000-007FFFh) not protected from Table Reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0001FFh) not protected from Table Reads executed in other blocks)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#define _XTAL_FREQ 20000000
/* Constants */

#define dataTX TXREG

/* Prototype functions for UART comunication. */

void txInitUART();                  // Function init UART (Asyncronus)
void txDisabled();                  // Disabled transmission for TX.
void txWrite(unsigned char data);    // Write data.




void txInitUART(){
    TXSTAbits.BRGH = 1;    // 1 is high speed in asyncronus
    SPBRG = 129;           // Select the baud rate with formula in datasheet.
    
    TXSTAbits.SYNC = 0;    // USART MODE SELECTION BIT, 0 is asyncronus mode. 
    RCSTAbits.SPEN = 1;    // SERIAL PORT ENABLED BIT, Serial port enabled (configures RC7/RX/DT and RC6/TX/CK pins as serial port pins)
    
    TRISC6 = 1;  // As stated in the datasheet
    TRISC7 = 1;  // As stated in the datasheet
    TXSTAbits.TXEN = 1;    // Transmit Enable bit, 1 = Transmit enabled.
    
}

void txDisabled(){
    TXSTAbits.TXEN = 0;    // Transmit Enable bit, 0 = Transmit disabled.
}

void txWrite(unsigned char data){
    while(!TRMT){
        continue;           // TXIF is flag to the bus TXREG, when TEXREG is empty TXIF is set.
    }
    dataTX = data;          // USART TXREG register for data.
}

void txWriteString(char *st){
    // "1234\"
    
    for(int w=0; st[w] != '\0'; w++){
        txWrite(st[w]);
    }
}

void boot(){
    TRISD = 0xFF;
}

int main(){
    boot();
    unsigned int cont = 0;
    txInitUART();
    char stringx[] = "12";
    char popo[30];                  // mejor definir tama�o arreglos en PIC.
    
    while (1){
        __delay_ms(700);

        txWriteString(stringx);
        
        cont++;
        
        if(cont == 10){
            stringx[0] = '6';
            stringx[1] = '5';
            cont = 0;
        }
        // txWrite(0);
        // __delay_ms(1200);

    }
    
    return 0;
}
