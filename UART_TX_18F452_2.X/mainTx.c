#pragma config OSC = HS         // Oscillator Selection bits (XT oscillator)
#pragma config PWRT = ON        // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOR = OFF        // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config LVP = OFF        // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <string.h>
#include <stdio.h>
#define _XTAL_FREQ 16000000
#include "lcd_i2c.h"
/* Constants */

#define dataTX TXREG

/* Global Vriables */

unsigned int cont_rc = 0;       // contador recibidor bits, flag data lista.
char rc_buff[100], rc_data[255], test_buff[30];
unsigned char data = 0;


/* Prototype functions for UART comunication. */
void init_uart(void);
void tx_stop();                  // Disabled transmission for TX.
void tx_start();
void rx_stop();                  // Disabled transmission for TX.
void rx_start();
void tx_write(unsigned char data);    // Write data.
void tx_write_string(char *st);
void init_interrupts(void);

/* NOTAS
    * TXI is Trasmit interrupt, enable with TXIE

    * TXIF (PIR1<4>) set when TXREG is empty, TXREG is used to store the data in software, when TXREG is 
    full in one Tcy the TSR is charge with data in TXREG in this moment TXREG is empty and TXIF is set.

    * TMRT is a bit only read, set when data in TSR is empty, clear when data storege in register 
    no have interrupt logic.



*/

void init_interrupts(void){
    RCONbits.IPEN = 0;    // disabled interrupts with mask.
    INTCONbits.GIE = 1;   // enabled unmasked interrupts.
    INTCONbits.PEIE = 1;  // enabled pheriperal interrupts unmasked.
    
    // config interrupts for USART.
    PIE1bits.TXIE = 0;   // disabled Trasmit interrupt.
    PIE1bits.RCIE = 1;   // Enabled reception interrupt.
       
}

void rx_start (void){
    RCSTAbits.CREN = 1; 
}

void rx_stop (void){
    RCSTAbits.CREN = 0;
}

void init_uart(void){
    TXSTAbits.BRGH = 1;    // 1 is high speed in asyncronus
    SPBRG = 103;           // Select the baud rate with formula in datasheet.
    
    TXSTAbits.SYNC = 0;    // USART MODE SELECTION BIT, 0 is asyncronus mode. 
    RCSTAbits.SPEN = 1;    // SERIAL PORT ENABLED BIT, Serial port enabled (configures RC7/RX/DT and RC6/TX/CK pins as serial port pins)
    
    TRISC6 = 1;  // As stated in the datasheet
    TRISC7 = 1;  // As stated in the datasheet
    tx_start();
    rx_start();
}

void tx_start(void){
    TXSTAbits.TXEN = 1;    // Transmit Enable bit, 1 = Transmit enabled.
}

void tx_stop(void){
    TXSTAbits.TXEN = 0;    // Transmit Enable bit, 0 = Transmit disabled.
}

void tx_write(unsigned char data){
    // TRMT bits is set when TSR is empty.
    
    while(!TXSTAbits.TRMT){
        continue;           // TXIF is flag to the bus TXREG, when TEXREG is empty TXIF is set.
    }
    
    dataTX = data;          // USART TXREG register for data.
}

void tx_write_string(char *st){
    // "1234\"
    /*
        Probar setear el flag para ver si inicia la interrupcion.
        y desde el ISR gestionar el envio de dato.

        * cuando se setea TXEN se habilita la trasmision, quiza cuando se habilite
        se empieze  aactivar la interrupcion.

    */
        for(int w=0; st[w] != '\0'; w++){
        tx_write(st[w]);
    }
}

void boot(){
    init_interrupts();
    init_uart();
    I2C_Master_Init();
    LCD_Init(0x4E);
    TRISDbits.RD0 = 0;
}

int main(){
    boot();
    LCD_Set_Cursor(1,1);
    LCD_Write_String("P");
    LATDbits.LATD0 = 0;
    cont_rc = 0;
    
    while (1){
      
        LCD_Set_Cursor(1,1);
        LCD_Write_String(rc_data);
       
    }
    
    return 0;
}


void __interrupt() adint(void) {
    
    if(PIR1bits.RCIF == 1){
        // the RCREG is full. ready for read!
        PIR1bits.RCIF = 0;
        //LATDbits.LATD0 = 1;
        
        data = RCREG;
        //LCD_Set_Cursor(1,1);
        //sprintf(test_buff, "dato: %d", data);
        //LCD_Write_String(test_buff);
        
        
        
        
        //LCD_Set_Cursor(1,1);
        //LCD_Write_String(test_buff);
        
        
        
        switch(data){
            case '~':
                //LCD_Clear();
                strcpy(rc_data, rc_buff);
                /*
                for (int x = 0; x < 30; x++){
                    *(rc_buff + x) = NULL;
                }
                */
                
                cont_rc = 0;
                
                //tx_write_string(rc_data);
                
                break;
            /*    
            case '\r':
                break;
            case '\n':
                break;
            */    
            default:
                *(rc_buff + cont_rc) = data;
                cont_rc++;
                break;
        }
           
        
    }
    
}