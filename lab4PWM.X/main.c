/*
 * File:   main.c
 * Author: 23ft
 * Task: PWM.
 * Created on 16 de abril de 2022, 10:58 AM
 */

#pragma config OSC = HS  // Oscillator Selection bits (XT oscillator)
#pragma config PWRT = ON // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOR = OFF // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config WDT = OFF // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config LVP = OFF // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)

#define _XTAL_FREQ 20000000
// (PWM period / (Tcy * TMR2 prescale value)) - 1 = PR2
#define period_pr2(pwm_period, frec_osc, presc_tmr2) ((pwm_period / ((4 / frec_osc) * presc_tmr2))-1)
// PWM duty cycle / TOSC * (TMR2 prescale value) = (CCPR1L:CCP1CON<5:4>)
#define duty_ccp(pwm_period, pwm_duty, frec_osc, presc_tmr2) (((pwm_duty * pwm_period) / 100) / ((1 / frec_osc) * presc_tmr2))


#include <xc.h>
#include "lcd_i2c.h"

unsigned int mask = 0x03;
float pwm_p, frec_osc, tmr2_presc = 0;

void change_duty(float pwm_duty);                                           // change duty cycle in CCP.
void setPWM(float period_pwm, float pwm_duty, float freq_osc, float presc); // config and init PWM in CCP1

void setPWM(float period_pwm, float pwm_duty, float freq_osc, float presc)
{
    /*
    CCPxCON -> registro de seleccion modo, ademas de selecconar el DUTY CYCLE del PWM, puede ser CCP1CON o CCP2CON
    PR2 -> registro donde se almacena nuestro periodo PWM para realizar la comparacion con el TMR2.

     * Tosc (periodo oscilacion) = 1/Fosc
     * Tcy  (Ciclo de maquina)   = 4/Fosc

     * PWM period = [(PR2) + 1] * 4 * TOSC * (TMR2 prescale value)
     PWM period = [(PR2) + 1] * Tcy * (TMR2 prescale value)
     PWM period / Tcy * (TMR2 prescale value) = (PR2 + 1)
     (PWM period / (Tcy * TMR2 prescale value)) - 1 = PR2

     PWM duty cycle = (CCPR1L:CCP1CON<5:4>) * TOSC * (TMR2 prescale value)
     PWM duty cycle / TOSC * (TMR2 prescale value) = (CCPR1L:CCP1CON<5:4>)

     */

    // save data global in the module PWM.
    pwm_p = period_pwm;
    frec_osc = freq_osc;
    tmr2_presc = presc;
    
    
    unsigned int periodo = (unsigned int) period_pr2(period_pwm, freq_osc, presc);
    PR2 = periodo;
    
    // Dutycycle.
    // para un ciclo del 50% (204.91us) el valor de (CCPR1L:CCP1CON<5:4>) es:
    // (CCPR1L:CCP1CON<5:4>) = 256.
    // mask is 0b0000 0011 << 4 --> 0b0011 0000
    unsigned int duty_temp = (unsigned int) duty_ccp(period_pwm, pwm_duty, freq_osc, presc);
    mask &= duty_temp;
    mask = mask << 4;
    CCP1CON |= mask;
    
    CCPR1L = duty_temp >> 2;
    
    unsigned char valor_prueba_debug = 0x00;
    unsigned char valor_prueba_debug2 = 0x00;
    
    valor_prueba_debug |= CCP1CON;
    valor_prueba_debug2 |= CCPR1L;
    
    // CCP1 config how output
    TRISCbits.RC2 = 0;

    // Config TMR2 with T2CON
    // T2CONbits.TMR2ON(bit 2) is bit for set enable or reset disable the timer.
    // t2con 0b0000 0111 - presc 16 and not use postcaler.
    T2CON = 0x07;

    // CCP1 config PWM mode.
    CCP1CON |= 0x0F;
}

void change_duty(float pwm_duty)
{
    mask &= (int) duty_ccp(pwm_p, pwm_duty, frec_osc, tmr2_presc);
    mask = mask << 4;
    CCP1CON |= mask;
    CCPR1L = 256 >> 2;
}

void main(void)
{
    // RC2 pin is CCP1
    // set the PWM in CCP1 with:
    // setPWM(period prefer in fraccion con notacion, duty en porcentaje entero, frecuencia osc notacion, prescaler prefer in notacion)    
    I2C_Master_Init();
    LCD_Init(0x4E);

    TRISCbits.RC2 = 0;
    setPWM(1e0/5e3, 50e0, 20e6, 16e0);
    LCD_Set_Cursor(1,1);
    LCD_Write_String("PWM set...");

    while (1)
    {
        
    }
    return;
}
