/*
 * File:   main.c
 * Author: pipe
 *
 * Created on 25 de abril de 2022, 11:59 AM
 */

/*
 
 MOdulo PC8574 expansior de I/O I2C, se conecta a la LCD en configuracion 4 bits, se puede controlar
 backlight ya que el I/O-3 del PC8574 conuma un transistor que escursiona el pin K(catodo led) a tierra o no.
 
 Address is select with pad A0-A1-A2 is a LSB from address, the MSB is a fixed (bit 7- bit 3)
 */

#pragma config OSC = HS         // Oscillator Selection bits (XT oscillator)
#pragma config PWRT = OFF        // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOR = OFF        // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config LVP = OFF        // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)

#include <xc.h>
#define _XTAL_FREQ 20000000
#include "lcd_i2c.h"

void main(void) {
    I2C_Master_Init(); // SCL_D SDA_D configred how input
    LCD_Init(0x4E);    // Incializar el LCD, se pasa direccion I2C de el PC8574, 0x4E son BIT1-BIT7 slave 
                       //address, BIT0 is write(0) or read(1).
    LCD_Set_Cursor(1,1);
    LCD_Write_String("PIC18F452 DEVKIT");                       
    LCD_Set_Cursor(2,1);
    LCD_Write_String(" ...start!...");

    while (1){

    }

    return;
}
