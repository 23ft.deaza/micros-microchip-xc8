subtitle "Microchip MPLAB XC8 C Compiler v2.36 (Free license) build 20220127204148 Og9 "

pagewidth 120

	opt flic

	processor	18F452
include "/opt/microchip/xc8/v2.36/pic/include/proc/18f452.cgen.inc"
getbyte	macro	val,pos
	(((val) >> (8 * pos)) and 0xff)
endm
byte0	macro	val
	(getbyte(val,0))
endm
byte1	macro	val
	(getbyte(val,1))
endm
byte2	macro	val
	(getbyte(val,2))
endm
byte3	macro	val
	(getbyte(val,3))
endm
byte4	macro	val
	(getbyte(val,4))
endm
byte5	macro	val
	(getbyte(val,5))
endm
byte6	macro	val
	(getbyte(val,6))
endm
byte7	macro	val
	(getbyte(val,7))
endm
getword	macro	val,pos
	(((val) >> (8 * pos)) and 0xffff)
endm
word0	macro	val
	(getword(val,0))
endm
word1	macro	val
	(getword(val,2))
endm
word2	macro	val
	(getword(val,4))
endm
word3	macro	val
	(getword(val,6))
endm
gettword	macro	val,pos
	(((val) >> (8 * pos)) and 0xffffff)
endm
tword0	macro	val
	(gettword(val,0))
endm
tword1	macro	val
	(gettword(val,3))
endm
tword2	macro	val
	(gettword(val,6))
endm
getdword	macro	val,pos
	(((val) >> (8 * pos)) and 0xffffffff)
endm
dword0	macro	val
	(getdword(val,0))
endm
dword1	macro	val
	(getdword(val,4))
endm
clrc   macro
	bcf	status,0
endm
setc   macro
	bsf	status,0
endm
clrz   macro
	bcf	status,2
endm
setz   macro
	bsf	status,2
endm
skipnz macro
	btfsc	status,2
endm
skipz  macro
	btfss	status,2
endm
skipnc macro
	btfsc	status,0
endm
skipc  macro
	btfss	status,0
endm
pushw macro
	movwf postinc1
endm
pushf macro arg1
	movff arg1, postinc1
endm
popw macro
	movf postdec1,f
	movf indf1,w
endm
popf macro arg1
	movf postdec1,f
	movff indf1,arg1
endm
popfc macro arg1
	movff plusw1,arg1
	decfsz fsr1,f
endm
	global	__ramtop
	global	__accesstop
# 55 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PORTA equ 0F80h ;# 
# 200 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PORTB equ 0F81h ;# 
# 325 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PORTC equ 0F82h ;# 
# 492 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PORTD equ 0F83h ;# 
# 613 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PORTE equ 0F84h ;# 
# 725 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
LATA equ 0F89h ;# 
# 825 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
LATB equ 0F8Ah ;# 
# 937 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
LATC equ 0F8Bh ;# 
# 1049 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
LATD equ 0F8Ch ;# 
# 1161 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
LATE equ 0F8Dh ;# 
# 1213 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TRISA equ 0F92h ;# 
# 1218 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
DDRA equ 0F92h ;# 
# 1411 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TRISB equ 0F93h ;# 
# 1416 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
DDRB equ 0F93h ;# 
# 1651 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TRISC equ 0F94h ;# 
# 1656 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
DDRC equ 0F94h ;# 
# 1891 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TRISD equ 0F95h ;# 
# 1896 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
DDRD equ 0F95h ;# 
# 2113 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TRISE equ 0F96h ;# 
# 2118 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
DDRE equ 0F96h ;# 
# 2265 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PIE1 equ 0F9Dh ;# 
# 2342 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PIR1 equ 0F9Eh ;# 
# 2419 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
IPR1 equ 0F9Fh ;# 
# 2496 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PIE2 equ 0FA0h ;# 
# 2540 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PIR2 equ 0FA1h ;# 
# 2584 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
IPR2 equ 0FA2h ;# 
# 2628 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
EECON1 equ 0FA6h ;# 
# 2694 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
EECON2 equ 0FA7h ;# 
# 2701 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
EEDATA equ 0FA8h ;# 
# 2708 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
EEADR equ 0FA9h ;# 
# 2715 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
RCSTA equ 0FABh ;# 
# 2720 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
RCSTA1 equ 0FABh ;# 
# 2939 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TXSTA equ 0FACh ;# 
# 2944 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TXSTA1 equ 0FACh ;# 
# 3207 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TXREG equ 0FADh ;# 
# 3212 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TXREG1 equ 0FADh ;# 
# 3219 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
RCREG equ 0FAEh ;# 
# 3224 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
RCREG1 equ 0FAEh ;# 
# 3231 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SPBRG equ 0FAFh ;# 
# 3236 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SPBRG1 equ 0FAFh ;# 
# 3243 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
T3CON equ 0FB1h ;# 
# 3364 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR3 equ 0FB2h ;# 
# 3371 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR3L equ 0FB2h ;# 
# 3378 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR3H equ 0FB3h ;# 
# 3385 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCP2CON equ 0FBAh ;# 
# 3473 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCPR2 equ 0FBBh ;# 
# 3480 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCPR2L equ 0FBBh ;# 
# 3487 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCPR2H equ 0FBCh ;# 
# 3494 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCP1CON equ 0FBDh ;# 
# 3573 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCPR1 equ 0FBEh ;# 
# 3580 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCPR1L equ 0FBEh ;# 
# 3587 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
CCPR1H equ 0FBFh ;# 
# 3594 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
ADCON1 equ 0FC1h ;# 
# 3662 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
ADCON0 equ 0FC2h ;# 
# 3803 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
ADRES equ 0FC3h ;# 
# 3810 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
ADRESL equ 0FC3h ;# 
# 3817 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
ADRESH equ 0FC4h ;# 
# 3824 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SSPCON2 equ 0FC5h ;# 
# 3886 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SSPCON1 equ 0FC6h ;# 
# 3956 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SSPSTAT equ 0FC7h ;# 
# 4213 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SSPADD equ 0FC8h ;# 
# 4220 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
SSPBUF equ 0FC9h ;# 
# 4227 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
T2CON equ 0FCAh ;# 
# 4298 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PR2 equ 0FCBh ;# 
# 4303 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
MEMCON equ 0FCBh ;# 
# 4408 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR2 equ 0FCCh ;# 
# 4415 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
T1CON equ 0FCDh ;# 
# 4522 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR1 equ 0FCEh ;# 
# 4529 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR1L equ 0FCEh ;# 
# 4536 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR1H equ 0FCFh ;# 
# 4543 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
RCON equ 0FD0h ;# 
# 4686 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
WDTCON equ 0FD1h ;# 
# 4714 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
LVDCON equ 0FD2h ;# 
# 4772 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
OSCCON equ 0FD3h ;# 
# 4792 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
T0CON equ 0FD5h ;# 
# 4862 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR0 equ 0FD6h ;# 
# 4869 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR0L equ 0FD6h ;# 
# 4876 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TMR0H equ 0FD7h ;# 
# 4883 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
STATUS equ 0FD8h ;# 
# 4954 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR2 equ 0FD9h ;# 
# 4961 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR2L equ 0FD9h ;# 
# 4968 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR2H equ 0FDAh ;# 
# 4975 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PLUSW2 equ 0FDBh ;# 
# 4982 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PREINC2 equ 0FDCh ;# 
# 4989 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
POSTDEC2 equ 0FDDh ;# 
# 4996 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
POSTINC2 equ 0FDEh ;# 
# 5003 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INDF2 equ 0FDFh ;# 
# 5010 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
BSR equ 0FE0h ;# 
# 5017 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR1 equ 0FE1h ;# 
# 5024 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR1L equ 0FE1h ;# 
# 5031 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR1H equ 0FE2h ;# 
# 5038 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PLUSW1 equ 0FE3h ;# 
# 5045 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PREINC1 equ 0FE4h ;# 
# 5052 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
POSTDEC1 equ 0FE5h ;# 
# 5059 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
POSTINC1 equ 0FE6h ;# 
# 5066 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INDF1 equ 0FE7h ;# 
# 5073 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
WREG equ 0FE8h ;# 
# 5085 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR0 equ 0FE9h ;# 
# 5092 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR0L equ 0FE9h ;# 
# 5099 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
FSR0H equ 0FEAh ;# 
# 5106 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PLUSW0 equ 0FEBh ;# 
# 5113 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PREINC0 equ 0FECh ;# 
# 5120 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
POSTDEC0 equ 0FEDh ;# 
# 5127 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
POSTINC0 equ 0FEEh ;# 
# 5134 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INDF0 equ 0FEFh ;# 
# 5141 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INTCON3 equ 0FF0h ;# 
# 5233 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INTCON2 equ 0FF1h ;# 
# 5310 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INTCON equ 0FF2h ;# 
# 5315 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
INTCON1 equ 0FF2h ;# 
# 5542 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PROD equ 0FF3h ;# 
# 5549 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PRODL equ 0FF3h ;# 
# 5556 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PRODH equ 0FF4h ;# 
# 5563 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TABLAT equ 0FF5h ;# 
# 5572 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TBLPTR equ 0FF6h ;# 
# 5579 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TBLPTRL equ 0FF6h ;# 
# 5586 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TBLPTRH equ 0FF7h ;# 
# 5593 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TBLPTRU equ 0FF8h ;# 
# 5602 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PCLAT equ 0FF9h ;# 
# 5609 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PC equ 0FF9h ;# 
# 5616 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PCL equ 0FF9h ;# 
# 5623 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PCLATH equ 0FFAh ;# 
# 5630 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
PCLATU equ 0FFBh ;# 
# 5637 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
STKPTR equ 0FFCh ;# 
# 5743 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TOS equ 0FFDh ;# 
# 5750 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TOSL equ 0FFDh ;# 
# 5757 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TOSH equ 0FFEh ;# 
# 5764 "/opt/microchip/xc8/v2.36/pic/include/proc/pic18f452.h"
TOSU equ 0FFFh ;# 
	debug_source C
	FNROOT	_main
psect	idataCOMRAM,class=CODE,space=0,delta=1,noexec
global __pidataCOMRAM
__pidataCOMRAM:
	file	"/home/pipe/MPLABXProjects/mcuXC8/lab4PWM.X/main.c"
	line	21

;initializer for _mask
	dw	(03h)&0ffffh
	global	_T2CONbits
_T2CONbits	set	0xFCA
	global	_TRISCbits
_TRISCbits	set	0xF94
	global	_CCP1CON
_CCP1CON	set	0xFBD
	global	_PR2
_PR2	set	0xFCB
	global	_T2CON
_T2CON	set	0xFCA
	global	_CCPR1L
_CCPR1L	set	0xFBE
; #config settings
	config pad_punits      = on
	config apply_mask      = off
	config ignore_cmsgs    = off
	config default_configs = off
	config default_idlocs  = off
	config OSC = "HS"
	config PWRT = "ON"
	config BOR = "OFF"
	config WDT = "OFF"
	config LVP = "OFF"
	file	"makeFiles/main.as"
	line	#
psect	cinit,class=CODE,delta=1,reloc=2
global __pcinit
__pcinit:
global start_initialization
start_initialization:

global __initialization
__initialization:
psect	bssCOMRAM,class=COMRAM,space=1,noexec,lowdata
global __pbssCOMRAM
__pbssCOMRAM:
	global	_tmr2_presc
_tmr2_presc:
       ds      3
	global	_frec_osc
_frec_osc:
       ds      3
	global	_pwm_p
_pwm_p:
       ds      3
psect	dataCOMRAM,class=COMRAM,space=1,noexec,lowdata
global __pdataCOMRAM
__pdataCOMRAM:
	file	"/home/pipe/MPLABXProjects/mcuXC8/lab4PWM.X/main.c"
	line	21
	global	_mask
_mask:
       ds      2
	file	"makeFiles/main.as"
	line	#
psect	cinit
; Initialize objects allocated to COMRAM (2 bytes)
	global __pidataCOMRAM
	; load TBLPTR registers with __pidataCOMRAM
	movlw	low (__pidataCOMRAM)
	movwf	tblptrl
	movlw	high(__pidataCOMRAM)
	movwf	tblptrh
	movlw	low highword(__pidataCOMRAM)
	movwf	tblptru
	tblrd*+ ;fetch initializer
	movff	tablat, __pdataCOMRAM+0		
	tblrd*+ ;fetch initializer
	movff	tablat, __pdataCOMRAM+1		
	line	#
; Clear objects allocated to COMRAM (9 bytes)
	global __pbssCOMRAM
lfsr	0,__pbssCOMRAM
movlw	9
clear_0:
clrf	postinc0,c
decf	wreg
bnz	clear_0
psect cinit,class=CODE,delta=1
global end_of_initialization,__end_of__initialization

;End of C runtime variable initialization code

end_of_initialization:
__end_of__initialization:
movlb 0
goto _main	;jump to C main() function
psect	cstackCOMRAM,class=COMRAM,space=1,noexec,lowdata
global __pcstackCOMRAM
__pcstackCOMRAM:
?_main:	; 1 bytes @ 0x0
??_main:	; 1 bytes @ 0x0
	ds   2
;!
;!Data Sizes:
;!    Strings     0
;!    Constant    0
;!    Data        2
;!    BSS         9
;!    Persistent  0
;!    Stack       0
;!
;!Auto Spaces:
;!    Space          Size  Autos    Used
;!    COMRAM          127      2      13
;!    BANK0           128      0       0
;!    BANK1           256      0       0
;!    BANK2           256      0       0
;!    BANK3           256      0       0
;!    BANK4           256      0       0
;!    BANK5           256      0       0

;!
;!Pointer List with Targets:
;!
;!    None.


;!
;!Critical Paths under _main in COMRAM
;!
;!    None.
;!
;!Critical Paths under _main in BANK0
;!
;!    None.
;!
;!Critical Paths under _main in BANK1
;!
;!    None.
;!
;!Critical Paths under _main in BANK2
;!
;!    None.
;!
;!Critical Paths under _main in BANK3
;!
;!    None.
;!
;!Critical Paths under _main in BANK4
;!
;!    None.
;!
;!Critical Paths under _main in BANK5
;!
;!    None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;!
;!Call Graph Tables:
;!
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (0) _main                                                 2     2      0       0
;!                                              0 COMRAM     2     2      0
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 0
;! ---------------------------------------------------------------------------------
;!
;! Call Graph Graphs:
;!
;! _main (ROOT)
;!

;! Address spaces:

;!Name               Size   Autos  Total    Cost      Usage
;!BIGRAM             5FF      0       0      21        0.0%
;!EEDATA             100      0       0       0        0.0%
;!BITBANK5           100      0       0      14        0.0%
;!BANK5              100      0       0      15        0.0%
;!BITBANK4           100      0       0      12        0.0%
;!BANK4              100      0       0      13        0.0%
;!BITBANK3           100      0       0      10        0.0%
;!BANK3              100      0       0      11        0.0%
;!BITBANK2           100      0       0       8        0.0%
;!BANK2              100      0       0       9        0.0%
;!BITBANK1           100      0       0       6        0.0%
;!BANK1              100      0       0       7        0.0%
;!BITBANK0            80      0       0       4        0.0%
;!BANK0               80      0       0       5        0.0%
;!BITCOMRAM           7F      0       0       0        0.0%
;!COMRAM              7F      2       D       1       10.2%
;!BITBIGSFRh          34      0       0      16        0.0%
;!BITBIGSFRllh        28      0       0      18        0.0%
;!BITBIGSFRlll        14      0       0      19        0.0%
;!BITBIGSFRlh          B      0       0      17        0.0%
;!BIGSFR               0      0       0     200        0.0%
;!BITSFR               0      0       0     200        0.0%
;!SFR                  0      0       0     200        0.0%
;!STACK                0      0       0       2        0.0%
;!NULL                 0      0       0       0        0.0%
;!ABS                  0      0       D      20        0.0%
;!DATA                 0      0       D       3        0.0%
;!CODE                 0      0       0       0        0.0%

	global	_main

;; *************** function _main *****************
;; Defined at:
;;		line 82 in file "/home/pipe/MPLABXProjects/mcuXC8/lab4PWM.X/main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5
;;      Params:         0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0
;;      Temps:          2       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	text0,class=CODE,space=0,reloc=2,group=0
	file	"/home/pipe/MPLABXProjects/mcuXC8/lab4PWM.X/main.c"
	line	82
global __ptext0
__ptext0:
psect	text0
	file	"/home/pipe/MPLABXProjects/mcuXC8/lab4PWM.X/main.c"
	line	82
	
_main:
;incstack = 0
	callstack 31
	line	85
	
l33:
	line	87
	
l784:
	asmopt push
asmopt off
movlw  96
movwf	(??_main+0+0+1)^00h,c
movlw	47
movwf	(??_main+0+0)^00h,c
	movlw	71
u17:
	dw	0FFFFh; errata NOP
decfsz	wreg,f
	bra	u17
	decfsz	(??_main+0+0)^00h,c,f
	bra	u17
	decfsz	(??_main+0+0+1)^00h,c,f
	bra	u17
	nop2
asmopt pop

	line	88
	
l786:
	bcf	((c:4042))^0f00h,c,2	;volatile
	line	89
	asmopt push
asmopt off
movlw  96
movwf	(??_main+0+0+1)^00h,c
movlw	47
movwf	(??_main+0+0)^00h,c
	movlw	71
u27:
	dw	0FFFFh; errata NOP
decfsz	wreg,f
	bra	u27
	decfsz	(??_main+0+0)^00h,c,f
	bra	u27
	decfsz	(??_main+0+0+1)^00h,c,f
	bra	u27
	nop2
asmopt pop

	line	90
	
l788:
	bsf	((c:4042))^0f00h,c,2	;volatile
	goto	l33
	global	start
	goto	start
	callstack 0
	line	93
GLOBAL	__end_of_main
	__end_of_main:
	signat	_main,89
	GLOBAL	__activetblptr
__activetblptr	EQU	0
	psect	intsave_regs,class=BIGRAM,space=1,noexec
	PSECT	rparam,class=COMRAM,space=1,noexec
	GLOBAL	__Lrparam
	FNCONF	rparam,??,?
	GLOBAL	___rparam_used
	___rparam_used EQU 1
	GLOBAL	___param_bank
	___param_bank EQU 0
GLOBAL	__Lparam, __Hparam
GLOBAL	__Lrparam, __Hrparam
__Lparam	EQU	__Lrparam
__Hparam	EQU	__Hrparam
	end
