/*
 * File:   dispMux.c
 * Author: pipe
 
 * Created on 7 de noviembre de 2021, 05:00 AM
 * > Library for control displays in module with PIC16F628A
 
 
 */


 
/* Prototypes Functions */
void muxInit();                               // Inicializacion modo MuxDisplays with PIC16F628A.
void muxNumber(unsigned long num,unsigned d);      // Sepárador de numero en unidades y decenas para visualizar. 
void muxView(unsigned digitsView);                                // Mostrar numero en displays.

/* Display 7 seg const */
#define segA2F PORTB            // Segments A to F
#define segG PORTAbits.RA0      // Segment G

#define D1 PORTAbits.RA2        // control encendido display 1 (decenas)
#define D0 PORTAbits.RA1        // control encendido display 0 (unidades)

unsigned long uni, dec;

unsigned char numbers[10][2] = {{0xF9, 0},  // Num0
                                 {0x18, 0}, // Num1
                                 {0x69, 1}, // Num2
                                 {0x39, 1}, // Num3
                                 {0x98, 1}, // Num4
                                 {0xB1, 1}, // Num5
                                 {0xF1, 1}, // Num6
                                 {0x19, 0}, // Num7
                                 {0xF9, 1}, // Num8
                                 {0xB9, 1}};// Num9


void muxInit() {
    TRISA = 0x00;   // TRISA A1-A2 are outputs.
    TRISB = 0x06;   // TRISB B1-B2 are inputs for USART.
    CMCON = 0x07;   // RA0-RA3 Digital I/O
}

void muxView(unsigned digitsView){
    D1 = 0;
    D0 = 1;
    
    segA2F = numbers[uni][0];
    segG = numbers[uni][1];
    __delay_ms(100);
    
    D1 = 1;
    D0 = 0;
    
    segA2F = numbers[dec][0];
    segG = numbers[dec][1];
    __delay_ms(100);
    
}

void muxNumber(unsigned long num, unsigned numDigits){
    static unsigned long res;
    dec = (num - (num % 10)) / 10;
    res = num % 10;
    uni = res;
    muxView(numDigits);
}